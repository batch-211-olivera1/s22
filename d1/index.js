console.log("Hello World");

// Array Methods

// JS has built-in functions and methods for arrays. this allows us to manipulate and access array items

// Mutator Methods
	/*
		-functions that  "mutate" or change an array after they're created
		-manipulate the original array performing various tasks such as adding and removing elements


	*/

	let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

	// push()
	/*
		-adds element in the end of an array and returns the arrays length
		Syntax:
			arrayName.push();
	*/

	console.log("Current Array: ");
	console.log(fruits);
	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits);

	fruits.push("Avocado", "Guava");
	console.log("Mutated array from push method: ");
	console.log(fruits);

	// pop()
	/*
		-Removes the last element in an array and returns the removed element
		Syntax:
			arrayName.pop();

	*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from pop method: ")
	console.log(fruits);

	// Mini activity


let ghostFighters = ["Eugene", "Dennis", "Alfred", "Taguro"];
console.log(ghostFighters);
	function unfriend(){
		ghostFighters.pop();
};
unfriend();
console.log(ghostFighters);



// unshift()
	/*
		-adds one or more elements at the beginning of an array
		Syntax:

		arrayName.unshift("elementA");
		arrayName.unshift("elementB");

	*/

	fruits.unshift("Lime", "Banana");
	console.log("Mutated array from unshift method: ");
	console.log(fruits);

	// shift()

	/*
		-removes an element at the beginning of an array and returns the removed element

		Syntax:
		arrayName.shift();

	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ");
	console.log(fruits);

	// splice

	/*
		-simultaneously removes elements from a specified index number and adds elements
		Syntax:

			arrayName.splice(StartingIndex,deleteCount,elementsToBeAdded);

	*/

	fruits.splice(1,2,"Lime","Cherry");
	console.log("Mutated array from splice method");
	console.log(fruits);

	// sort()
	/*
		-Rearranges the array elements in alphanumeric order
		-Syntax:
			arrayName.sort();

	*/

	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);

	// reverse
	/*
		-Reverses the order of array elements
		-Syntax:
		arrayName.reverse();

	*/

	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);


// Non-Mutator Methods
	/*
		-functions that do not modify or change an array after they are created
		-these method do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output

	*/	

	let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

	console.log(countries);
	// indexOf()
	/*
		-returns the index number of the first matching element found in an array
		-if no match is found the result is -1
		-search process will be done from first element proceeding to the last element
		-Syntax:
			arrayName.indexof(searchValue);
			arrayName.indexof(searchValue,fromIndex);

	*/

	let firstIndex= countries.indexOf("FR",3);
	console.log("Result of indexOf method: " + firstIndex);

	let invalidCountry = countries.indexOf("BR");
	console.log("Result of indexOf method: " + invalidCountry);

	// lastIndexOf()
	/*
		-returns the index number of the last matching element found in an array
		-search process will be done from the last element proceeding to the first element
		-Syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, fromIndex);

	*/

	let lastIndex = countries.lastIndexOf("PH",3);
	console.log("Result of lastIndexOf method: " + lastIndex);

	// slice()
	/*
		-portions/slices elements from an array and returns a new array
		-Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex,endingIndex);
	*/

	let slicedArrayA = countries.slice(2);
	console.log("Result from slice method: ");
	console.log(slicedArrayA);

	let slicedArrayB = countries.slice(2,4);
	console.log("Result from slice method: ");
	console.log(slicedArrayB);

	let slicedArrayC = countries.slice(-4);
	console.log("Result from slice method: ");
	console.log(slicedArrayC);

	// toString();
	/*
		-Returns an array as a string separeted by commas
		-Syntax:
		arrayName.toString();
	
	*/
	let stringArray = countries.toString();
	console.log("Result from toString method: ");
	console.log(stringArray);

	// concat()
	/*
		-combines two arrays and returns the combined result
		-Syntax:
			arrayA.concat(arrayB);
			arrayA.concat(elementA);

	*/

	let taskArrayA = ["drink html", "eat javascript"];
	let taskArrayB = ["inhale css", "breath sass"];
	let taskArrayC = ["get git", "be node"];

	let tasks = taskArrayA.concat(taskArrayB);
	console.log("Result from concat method: ");
	console.log(tasks);

	// Combine multiple arrays

	let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
	console.log(allTasks);

	// Combining arrays with elements
	let combinedTasks = taskArrayA.concat("Smell express", "throw react");
	console.log("Result from concat method: ");
	console.log(combinedTasks);

	// join()

		/*
			-returns an array as a string separated by specified separator
			-Syntax:
			arrayName.join("separatorString");

		*/

		let users = ["John", "Jane", "Joe", "Robert", "Nej"];
		console.log(users.join());
		console.log(users.join(""));
		console.log(users.join(" - "));


// Iteration Method

	/*
		-loops designed to perform repetitive task on arrays
		-loops over all items in an array
		-useful for manipulating array data resulting in complex tasks

	*/

// forEach()
/*
	-similar to a for loop that iterates on each array element
	-for each item in the  array, the anonymous function passed in the forEach() method will be run
	-the anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
	-variable names for arrays are normally written in the plural form of the data stored in an array
	-it's common practice to use the singular form of the array content for parameter names used in array loops
	-forEach() does not return anything
	-Syntax:
		arrayName.forEach(function(indivElement){
	
		})

*/

allTasks.forEach(function(task){
	console.log(task);
})


// mini activity

function displayGhostFighters(fighter){
	ghostFighters.forEach(function(fighter){
	console.log("forEach method: ")
	console.log(fighter);
})
}
displayGhostFighters()

// Using forEach() with conditional statements


// Looping through all array items
/*
	-it is a good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

	creating a separate variable to store results of an array iteration iteration methods

*/

let filteredTasks = [];

allTasks.forEach(function(task){
	console.log(task)
	if(task.length>10){
		filteredTasks.push(task)
	}
});
console.log("Result of filtered tasks: ");
console.log(filteredTasks);


// map()
/*
	-iterates on each element and returns new array with different values depending on the result of the function's operation

	-Syntax:
		let/const resultArray = arrayName.map(function(individualElement))

*/

let numbers = [1,2,3,4,5];
let numberMap = numbers.map(function(number){
		return number * number;
})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ")
console.log(numberMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
})
console.log(numberForEach);


// every()
/*
	-checks if all elements in an array meet the given conditions
	-this is useful for validating data stored in arrays specially when dealing with large amounts of data
	-returns true if all elements met the condition, otherwise false
	-Syntax:

	let/const resultArray = arrayName.every(function(indivElement){
	return expression/condition;
	})

*/

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log("Result of every method: ");
console.log(allValid);

// some()
/*
	-checks if at least one element in the array meets the given condition
	-return a true value if at least one element meets the condition and flase if otherwise
	-Syntax:
	let/const resultArray = arrayName.some(function(indivElement){
		return expression/condition;

	})

*/

let someValid = numbers.some(function(number){
	return (number<2);
})
console.log("Result of some method: ");
console.log(someValid);

// Combining the returned result from every/some method may used in other statements to perform consecutive results
if(someValid){
		console.log("Some numbers in the array are greater than 2 " + someValid);
}

// filter
/*
	-returns a new array that contains elements which meets a given condition
	-returns an empty array if no elements were found
	-Syntax:

	let/const resultArray = arrayName.filter(function(indivElement){
		return expression/condition;
		})
*/

let filterValid = numbers.filter(function(number){
	return (number<3);
})
console.log("Result of filter method: ");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
})
console.log("Result of filter method");
console.log(nothingFound);

// filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	// console.log(number);
	if(number<3){
			filteredNumbers.push(number);
	}
})
console.log("Result of filter method: ");
console.log(filteredNumbers);

// includes()
/*
	-checks if the argument passed can be found in the array
	-it returns a boolean which can also be saved in a variable
	-returns true if the argument is found in the array
	-returns false if it is not
	-Syntax:
		arrayName.includes(<argumentToFind>)

*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor",];

let productFound = products.includes("Mouse");
console.log(productFound)

let productNotFound = products.includes("Headset");
console.log(productNotFound);

// Method Chaining
	/*act
		-methods can be chained using one after another
		-the result of the first method is used on the second method until all "chained" methods have been resolved

	*/

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes("a");
	});

	console.log(filteredProducts);

// mini activity

let contacts = ["Ash"];

function addTrainer(trainer){
	let doesTrainerExist = contacts.includes(trainer)
	if(doesTrainerExist){
		alert("already added");
	}else{
		contacts.push(trainer);
		alert("Registered");
	}
}



// reduce

/*
	-evaluates elements from left to right and returns/reduces the array into a single value
	-Syntax:
		let/const resultArray = arrayName.reduce(function(accumulator,currenValue){
	return expression/operation
		})

	-"Accumulator" parameter in the function stores the result for every iteration of the loop
	-"cerruntValue" is the current/next element in the array that is evaluated in each iteration of the loop

	-"reduce" method works
	1. the first/result element in the array is stored in the "accumulator" paramaeter
	2. second/next element in the array is stored
*/

console.log(numbers);
let iteration = 0;
let iterationStr = 0;

let reduceArray = numbers.reduce(function(x,y){
	console.warn("current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("current value: " + y);

	return x + y;
})
console.log("Result of reduce method: " + reduceArray);

let list = ["Hello", "Again", "World"];

let reduceJoin = list.reduce(function(x,y){
	console.warn("current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("current value: " + y);

	return x + " " + y;
})
console.log("Result of reduce method: " + reduceJoin);